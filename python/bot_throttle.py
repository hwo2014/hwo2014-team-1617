
import numpy as np
import throttleFunctions
import calculate_distance

# defines the throttle required for the 
def bot_throttle(self): 
	# gets the previous best throttles.
	lvls = self.prevLvls
	nums = self.prevNums
	
	if(self.turboInUse):
		tickIteration = 70
	else:
		tickIteration = self.tickIteration
	
	thrIteration  = self.thrIteration
	
	# initializes the distance and the best throttle
	bestDistance = 0
	bestThrottle = 0

	# randomization factors
	randomization = self.randomization
	rInt		  = self.randint
	
	# Defines, how close is the next curve
	pieceIndex = self.pieceIndex
	maxPieces  = self.numberOfPieces
	pieceDistance = self.pieceDistance
	velocity	  = self.speed
	isStraightPiece = 1
	curvePieceIndex  = pieceIndex
	curvePieceIndex2 = pieceIndex
	pieceIndex2	  = pieceIndex
	#if velocity == 0:
	#	return 1.0
	
	# finds the next curve piece index
	while isStraightPiece:
		curvePieceIndex = throttleFunctions.next_piece_index(curvePieceIndex, maxPieces)
		curvePieceIndex2= curvePieceIndex2 + 1
		if not self.pieces[curvePieceIndex][0]:
			isStraightPiece = False
			curveRadius	= self.pieces[curvePieceIndex][1][self.currentLane]

	# distance to the next curve
	distanceToCurve = self.piece_length(pieceIndex)-pieceDistance
	pieceIndex2 = pieceIndex2 + 1
	pieceIndex  = throttleFunctions.next_piece_index(pieceIndex, maxPieces)
	while pieceIndex2 < curvePieceIndex2:
		distanceToCurve = distanceToCurve + self.piece_length(pieceIndex)
		pieceIndex2 = pieceIndex2 + 1
		pieceIndex  = throttleFunctions.next_piece_index(pieceIndex, maxPieces)
		pieceIndex = self.pieceIndex

	# Estimates if there is need to increase ticks amounts
	difTicks = False
	if distanceToCurve / (velocity + 0.01) < tickIteration and curveRadius < 79 and tickIteration < 40:
		tickIteration = tickIteration/2+tickIteration
		thrIteration = 2*thrIteration/3
		difTicks = True
	nums = [tickIteration/3, tickIteration/3, (tickIteration-2*tickIteration/3)]
	bestLvls = lvls
	bestNums = nums
	throttles	 = throttleFunctions.create_throttles(nums,lvls)
	
	# The fast algorithm
	if self.useFastAlgo:
		#testLvls = [1.0,0.0,0.0]
		tickIteration = 70
		testNums = [1,tickIteration-1,0]
		#testThrottles = throttleFunctions.create_throttles(testNums,testLvls)
		for x in np.linspace(1,0,101):
			testLvls = throttleFunctions.multiply_list_and_scalar([1.0,0.0,0.0],x)
			testThrottles = throttleFunctions.create_throttles(testNums,testLvls)
			if calculate_distance.calculate_distance(self,testThrottles) :
				#print(str(calculate_distance.calculate_distance(self,testThrottles)))
				return testThrottles[0]
		#print(str(calculate_distance.calculate_distance(self,testThrottles)))
		return 0
	
	# Tests if full throttle is ok
	testLvls = [1.0,1.0,1.0]
	testThrottles = throttleFunctions.create_throttles(nums,testLvls)
	if calculate_distance.calculate_distance(self,testThrottles):
		return 1.0

	# iterates for the optimal throttle
	for iteration in range(thrIteration):
		distance = calculate_distance.calculate_distance(self,throttles)
		if distance > bestDistance:
			bestDistance = distance
			bestThrottle = throttles[0]
			bestLvls	 = lvls
			bestNums	 = nums
	
		# Randomizes new values
		lvls = throttleFunctions.sum_lists(bestLvls ,throttleFunctions.multiply_list_and_scalar(np.random.randn(len(lvls)),randomization))
		nums = throttleFunctions.sum_lists(bestNums,throttleFunctions.add_list_and_scalar(np.random.randint(rInt,size = len(nums)),-rInt/2-1)) 
	
		# Corrects them
		nums = throttleFunctions.correct_nums(nums,tickIteration)
		lvls = throttleFunctions.correct_lvls(lvls)
	
		# makes new throttles and so on
		throttles = throttleFunctions.create_throttles(nums,lvls)
	
	if not difTicks:
		self.prevLvls = bestLvls
		self.prevNums = bestNums
	return bestThrottle

