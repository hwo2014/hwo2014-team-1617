

import numpy as np
import throttleFunctions

# computes how far the car can travel with given throttle
def calculate_phis2(speeds, radius, angleCurve,Cs,phis2,dphis2):
    
    # Defines the initial parameters of the car
    tickIteration   = len(speeds)
#     checkAngle      = 0
#     v1              = 0
#     omega1          = 0
#     alpha1          = 0
#     a1              = 0
#     d1              = 0
#     phi1            = 0
    angularVelocity = 0
    angle           = 0
    
    # Defines, if the current piece has switch or not
    #if self.pieces[pieceIndex][4]:
    #    hasSwitch = 1
    #else:
    #    hasSwitch = 0
    #switches = self.currentLane
    
    # Defines the the length of the current piece taking into account the switch and if it is a curve
    #curPieceLength = self.piece_length(pieceIndex)
    #curPieceLength = curPieceLength+hasSwitch * 2.06
    #print('huomio!    '+ str(pieceIndex))
    
    #if len(self.pieces[pieceIndex][1]) > 0 :
    #    radius = self.pieces[pieceIndex][1][lane]
    #else :
    #    radius = 0
    
    # Defines if turbo is in use or not and defines th turbo factor
    #if self.turboInUse:
    #    turbo = True
    #    turboLeft = self.turboTicksLeft
    #    turboFactor = self.turboFactor
    #else:
    #    turbo = False
    #    turboLeft = 0
    #    turboFactor = 1
    phis = []
    # computes the distance of certain amount of tick iterations
    for tick in range(tickIteration):
        # updates the velocities etc.
        #acceleration = throttleFunctions.calculate_acceleration(self.Cv,self.CT,velocity,throttles[tick],turboFactor)
        #velocity = velocity + throttleFunctions.calculate_acceleration(self.Cv,self.CT,velocity,throttles[tick],turboFactor)
        #distance = distance + velocity
        #pieceDistance = pieceDistance + velocity
        angularAcceleration = throttleFunctions.calculate_angular_acceleration(Cs[0],Cs[1],Cs[2],Cs[3],speeds[tick],angle,angleCurve[tick],radius[tick],angularVelocity,Cs[0])
        angularVelocity = angularVelocity + angularAcceleration
        angle = angle + angularVelocity
        phis.append(angle)
#         if tick == 0 :
#             v1              = velocity
#             omega1          = angularVelocity
#             alpha1          = angularAcceleration
#             a1              = acceleration
#             d1              = distance
#             phi1            = angle
                 
#         if abs(angle)>abs(checkAngle):
#             checkAngle = angle
#         if abs(angle)>maxAngle:
            #if throttles[0]<0.001:
                #print('heimoi'+ ";" + str(phi1) + ";" + str(omega1) + ";" + str(alpha1)+";" + str(d1) + ";" + str(v1) + ";" + str(a1) )
#             return 0
        
        # checks if moved to different piece and finds the important values (radius of the piece etc.) similarly as initially
#         if curPieceLength < pieceDistance:
#             pieceDistance = pieceDistance - curPieceLength
#             pieceIndex = throttleFunctions.next_piece_index(pieceIndex,maxPieces)
#             #pieceIndex+1
#             #if pieceIndex = self.numberOfPieces:
#             #    pieceIndex = 0
#             if hasSwitch:
#                 lane = self.nextLane
#             if self.pieces[pieceIndex][4]:
#                 hasSwitch = 1
#             else:
#                 hasSwitch = 0
#             
#             if len(self.pieces[pieceIndex][1]) > 0 :
#                 radius = self.pieces[pieceIndex][1][lane]
#             else :
#                 radius = 0
#             angleCurve = self.pieces[pieceIndex][2]
#             curPieceLength = self.piece_length(pieceIndex)
#             curPieceLength = curPieceLength+hasSwitch * 2.06 * abs(lane-self.nextLane)
#         # updates turbo
#         if turbo:
#             turboLeft = turboLeft - 1
#             if turboLeft == 0:
#                 turboFactor = 1
#                 turbo = False
#     # returns the final distance traveled 
#     print('heimoi'+ ";" + str(phi1) + ";" + str(omega1) + ";" + str(alpha1)+";" + str(d1) + ";" + str(v1) + ";" + str(a1) )
    return phis