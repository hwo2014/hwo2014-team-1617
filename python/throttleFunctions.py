
import numpy as np

# important functions for the throttle calculations

# creates list of ones with length n
def onelistmaker(n):
    listofones = [1.0] * n
    return listofones

# creates throttles from nums(number of points in each "bulk") and lvls(heights of the "bulks")
def create_throttles(nums,lvls):
    ones = onelistmaker(nums[0])
    thr1 = [x * lvls[0] for x in ones] 
    ones = onelistmaker(nums[1])
    thr2 = [x * lvls[1] for x in ones]
    ones = onelistmaker(nums[2])
    thr3 = [x * lvls[2] for x in ones]
    thr = []
    thr.extend(thr1)
    thr.extend(thr2)
    thr.extend(thr3)
    return thr

# defines the force in curve (zeroForce is based on max speed without car turning into angle in curve )
def force_in_curve(velocity,radius,angleCurve,zeroForce,C4):
    if abs(radius) > 0 and (velocity ** 2/radius-zeroForce) > 0:
        return np.sign(angleCurve)*(velocity ** 2 / radius-zeroForce)
    else :
        return 0

# the function for the angular acceleration
def calculate_angular_acceleration(C1,C2,C3,zeroForce,velocity,angle,angleCurve,radius, angularVelocity,C4):
    return C3*force_in_curve(velocity,radius,angleCurve,zeroForce,C4)+C1*np.sign(angle)*abs(angle)**1+C2*np.sign(angularVelocity)*abs(angularVelocity)**1

# The acceleration
def calculate_acceleration(Cv,CT,velocity,throttle,turboFactor):
    return Cv*velocity+CT*throttle*turboFactor

# The next piece index
def next_piece_index(pieceIndex, totalPieces):
    return (pieceIndex + 1) % totalPieces

# Defines the length of the current piece, so that takes onto account the lane and curve
def piece_length(self, pieceIndex):
    if self.pieces[pieceIndex][0]:
        return self.pieces[pieceIndex][0]
    else:
        return self.pieces[pieceIndex][3][self.currentLane]

# Defines the sum of two lists
def sum_lists(list1,list2):
    return [list1[i]+list2[i] for i in range(len(list1))]

# Defines the production of list and scalar
def multiply_list_and_scalar(list1,scalar):
    return [x * scalar for x in list1] 

# Multiplies lists point wise
def multiply_lists(list1,list2):
    return [list1[i]*list2[i] for i in range(len(list1))] 

# Defines the addition of a list and a scalar
def add_list_and_scalar(list1,scalar):
    return [x + scalar for x in list1] 

# Corrects nums
def correct_nums(nums,tickIteration):
    for ind in find_values_less_than_zero(nums):
        nums[ind] = 0
    nums[-1] = 0
    if sum(nums)>tickIteration:
        nums[0] = 1
    if sum(nums)>tickIteration:
        nums[1] = 1
    nums[-1] = tickIteration-sum(nums)
    return nums

# Corrects lvls
def correct_lvls(lvls):
    for ind in find_values_less_than_zero(lvls):
        lvls[ind] = 0.0
    for ind in find_values_larger_than_one(lvls):
        lvls[ind] = 1.0
    return lvls

# Finds indices less than zero
def find_values_less_than_zero(list1):
    result = []
    i = 0
    length = len(list1)
    while i < length:
        x = list1[i]
        if x < 0:      
            result.append(i)
        i = i + 1 
    return result

# Finds indices larger than one
def find_values_larger_than_one(list1):
    result = []
    i = 0
    length = len(list1)
    while i < length:
        x = list1[i]
        if x > 1:      
            result.append(i)
        i = i + 1 
    return result

# guesses new Cs
def guessCs(currentCs, randomization ):
    result = []
    for i in range(len(currentCs)):
        result.append(currentCs[i]+np.random.randn()*randomization[i])
    #result.append(currentCs[1]+np.random.randn(1)*randomization[1])
    #result.append(currentCs[2]+np.random.randn(1)*randomization[2])
    #result.append(currentCs[3]+np.random.randn(1)*randomization[3])
    return result

# guesses new Cs v.2.0
def guessCs2(currentCs, randomization ):
    result = []
    randomFactor = np.random.randn()*randomization[1] + randomization[0]
    for i in range(len(currentCs)):
        result.append(currentCs[i]+np.random.randn()*randomFactor*abs(currentCs[i]))
    return result

# returns 
def compare(alphas,alphasInitCs,alphasGuessCs):
    #listInit = sum_lists(alphasInitCs,multiply_list_and_scalar(alphas,-1))
    #penaltyInit = sum(multiply_lists(listInit,listInit)) 
    #listGuess = sum_lists(alphasGuessCs,multiply_list_and_scalar(alphas,-1))
    #penaltyGuess = sum(multiply_lists(listGuess,listGuess))
    penaltyInit = penalty_function(alphas,alphasInitCs)
    penaltyGuess = penalty_function(alphas,alphasGuessCs)
    if penaltyGuess < penaltyInit :
        #print("s "+str(penaltyInit))
        return penaltyGuess
    else:
        return 0

# returns 
def penalty_function(alphas,alphasInitCs):
    listInit = sum_lists(alphasInitCs,multiply_list_and_scalar(alphas,-1))
    return sum(multiply_lists(listInit,listInit)) 
    #return sum(multiply_lists(multiply_lists(listInit,listInit),multiply_lists(listInit,listInit))) 
    