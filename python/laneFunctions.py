

def checkNextSwitch(self):

    sumAngleRight = 0
    sumAngleLeft = 0
    
    nextSwitchIndex = (self.pieceIndex % self.numberOfPieces) + 1
    if nextSwitchIndex >= len(self.pieces) - 1 :
        nextSwitchIndex = 0
        self.switchCalculateLap = self.switchCalculateLap + 1
    
    while self.pieces[nextSwitchIndex][4] == 0 :
        nextSwitchIndex = nextSwitchIndex + 1 
        if nextSwitchIndex >= self.numberOfPieces - 1 :
            nextSwitchIndex = 0
            self.switchCalculateLap = self.switchCalculateLap + 1
    
    #print("NEXT SWITCH INDEX : " + str(nextSwitchIndex))
        
    followingSwitchIndex = nextSwitchIndex + 1
    if followingSwitchIndex >= self.numberOfPieces - 1 :
        followingSwitchIndex = 0
    while self.pieces[followingSwitchIndex][4] == 0 :
        if( self.pieces[followingSwitchIndex][2] > 0 ) :
            sumAngleRight = sumAngleRight + abs( self.pieces[followingSwitchIndex][2] )
        elif ( self.pieces[followingSwitchIndex][2] < 0 ) :
            sumAngleLeft = sumAngleLeft + abs( self.pieces[followingSwitchIndex][2] )
        #Next index
        followingSwitchIndex = followingSwitchIndex + 1 
        if followingSwitchIndex >= self.numberOfPieces - 1 :
            followingSwitchIndex = 0
    
    #print("FOLLOWING SWITCH INDEX : " + str(followingSwitchIndex))
    #print("SUM Right: " + str(sumAngleRight) + ", Left: " + str(sumAngleLeft))
    
    self.switchCalculatedIndex = nextSwitchIndex # + self.pieceIndex - (self.pieceIndex % self.numberOfPieces)
        
    if(sumAngleRight > sumAngleLeft and self.nextLane < len(self.lanes) - 1 ):
        #print("SWITCH TO RIGHT")
        return [1, 0, -1]
        #self.switchLaneRight()
        #commandGiven = True
    elif( sumAngleLeft > sumAngleRight and self.nextLane > 0 ) :
        #print("SWITCH TO LEFT")
        return [-1, 0, 1]
        #self.switchLaneLeft()
        #commandGiven = True
    else:
        if self.nextLane < len(self.lanes) - 1 :
            return [0,1,-1]
        elif self.nextLane > 0 :
            return [0,-1,1]
    
    return [0,1,-1]


def switchBasedOnDecision(self,decision):
    if decision > 0 :
        self.switchLaneRight()
    elif decision < 0 :
        self.switchLaneLeft()
    return 0
        
