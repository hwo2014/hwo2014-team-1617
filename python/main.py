import json
import socket
import sys
import numpy as np
import throttleFunctions
import calculate_distance
import bot_throttle
import calculate_phis2
import enemyFunctions
import laneFunctions
import enemy_controller

class tttMasterBot(object):
    
    #Initialize bot
    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        #My car
        self.myname = ""
        self.mycolor = ""
        
        self.turboAvailable = False
        self.turboAvailableTicksMax = 0
        self.turboAvailableFactor = 0

        self.turboInUse = False
        self.turboFactor = 0
        self.turboTicksLeft = 0 
        #self.turboTicksUsed = 0
        
        self.currentLap = 1
        
        #Track information
        self.pieces = []
        self.lanes = {}
        self.numberOfPieces = 0
        #Straights
        self.maxStraightPieceIndex = 0
        self.straights = {}
        #Lap lengths on different lanes
        self.laplengths = []
        self.enemySlow = {}
        
        #Race information
        self.cars = {}
        self.laps = 0
        self.maxLapTimeMs = 0
        self.quickRace = False
        
        #parameter values (for throttle calculation)
        self.initC1 = -0.0067
        self.initC2 = -0.1304
        self.initC3 =  3.1268
        self.C1 = -0.0067#-0.0084
        self.C2 = -0.1304#-0.1049
        self.C3 =  2.8848
        self.C4 =  []
        self.zeroForce = 0.29#5.93*5.93/110.0
        #self.C1 = -0.0084
        #self.C2 = -0.1049
        #self.C3 =  2.8848
        #self.zeroForce = 5.93*5.93/110.0
        self.Cv = -0.02
        self.CT =  0.2
        self.maxAngle = 37
        self.tickIteration = 30
        self.thrIteration = 300
        self.prevNums = [20,10,10]
        self.prevLvls = [1,0.7,0.7]
        self.randomization = 0.02
        self.randint = 9 
        self.useFastAlgo = True
        self.randomizeCs = [0.0005,0.005,0.05,0.01,0.1,0.1,0.1]
        self.guesses     = 180
        self.use_matrix  = False
        self.iteration_amounts = 120
        self.C123_iterations = 0
        
        #Car 
        self.currentLane = 0
        self.nextLane = 0
        self.pieceDistance = 0
        self.pieceIndex = 0
        self.speed = 0
        self.acceleration = 0
        self.throttlevalue = 0
        self.angle = 0
        self.angularVelocity = 0
        self.angularAcceleration = 0
        self.ontrack = 1
        
        
        self.hasSetCT = False
        self.hasSetCv = False
        self.hasSetCv_ind = 0
        
        self.hasSetC123 = False
        self.setC123counter = 0
        self.setC123angle = []
        self.setC123angularVelocity = []
        self.setC123angularAcceleration = []
        self.setC123velocity = []
        self.setC123radius = [] 
        self.setC123angleCurve = []    
        self.lowerLimitsC123 = [-1.0,-10.0,-5,-5.0,0.0,0.0,0.0]
        self.upperLimitsC123 = [1.0,1.0,10.0,8.0,3.0,3.0,3.0]  
        self.amountOfEstimates = 65
        
        self.nextLaneDefaultDecision = [0, -1, 1]

        self.switchCalculatedIndex = -1
        self.switchTo = ""
        self.switchCalculateLap = self.currentLap
        
        self.gametickcalc = 0
    
        self.debugKeepZero = False
        
        self.onCarPositionsFirstTime = True
        
        #Enemy car information
        self.enemies = {}
        self.enemiesDnf = {}
        
    #Start race by joining and reading messages (and acting upon them)
    def run(self):
        self.join() #Basic join for keimola
        #Name of track ( "keimola" , "germany" ), Password ( "" for public), Number of cars in the race (1 for just my own car)
        #self.create_race("keimola", "munsanaonlaki", 2)
        #self.join_race(2, "keimola", "munsanaonlaki")
        #self.create_race("germany", "abcde", 1) 
        
        #self.create_race("usa", "abcde1234567", 2)
        #self.join_race(2, "usa", "abcde1234567")
        
        #self.create_race("france", "abcde1234567", 1)
        self.msg_loop()
        
    # *** Actions based on message received from server ***
    
    # * Beginning a race *
    
    def on_join(self, data):
        print("Joined")
        #self.ping()
    
    def on_your_car(self, data):
        self.mycolor = data['color']
        self.myname = data['name']
        #print("My car, name: " + data['name'] + " and color: " + data['color'] )
        #self.ping()
    
    def on_game_start(self, data):
        print("Race started")
        self.ping()
    
    def on_game_init(self, data):
        
        #print("Track info begins")
        
        #print("Lanes index;distancefromcenter")
        # Read the lanes
        if 'lanes' in data['race']['track']: #Check if lane information was given
            n_lanes = len(data['race']['track']['lanes']) #Get number of lanes
            if n_lanes > 0: #If there are lanes
                for lane in data['race']['track']['lanes']: #Loop through lanes
                    self.lanes[lane['index']] = lane['distanceFromCenter'] #Remember the distance from center
                    #print(str(lane['index']) + ";" + str(lane['distanceFromCenter'])) #Print lane index and distance
            else: # wtf? single-lane track?
                self.lanes[0] = 0
        else:   # wtf? single-lane track?
            self.lanes[0] = 0
        
        #print("Pieces")
        pieceIndex = 0  
        # Read the pieces into matrix
        if 'pieces' in data['race']['track']:
            for piece in data['race']['track']['pieces']: #Loop through all track pieces
                lanelengths = {} #Dictionary for curve lengths for different lanes
                radiuslanes = {}
                
                #If piece is a straight, it will have only length
                if 'length' in piece:
                    length = piece['length']
                    for laneIndex in self.lanes :
                        lanelengths[laneIndex] = length
                else:
                    length = 0
                
                #If piece is a bend, it will have radius and angle information
                
                if 'radius' in piece and 'angle' in piece:
                    radius = piece['radius']
                    angle = piece['angle']
                    for laneIndex in self.lanes : #Calculate lengths for different lanes
                        # Angle (degrees) / 360 (degrees) * 2 * (Radius + Lane distance from radius ) * Pi
                        if angle > 0 : #Turn to right (lane distance is negative on left hand side of track and positive on right hand side) 
                            lanelengths[laneIndex] = abs(angle) / 360 * 2 * (radius - self.lanes[laneIndex]) * 3.141592653
                        else : #Turn to left
                            lanelengths[laneIndex] = abs(angle) / 360 * 2 * (radius + self.lanes[laneIndex]) * 3.141592653
                    
                    for laneIndex in self.lanes : #Calculate radiuses for different lanes
                        # Angle (degrees) / 360 (degrees) * 2 * (Radius + Lane distance from radius ) * Pi
                        if angle > 0 : #Turn to right (radius is negative on left hand side of track and positive on right hand side) 
                            radiuslanes[laneIndex] = radius - self.lanes[laneIndex]
                        else : #Turn to left
                            radiuslanes[laneIndex] = radius + self.lanes[laneIndex]
                        
                        #DEBUG print("Piece: " + str(len(self.pieces) + 1) + " - " + str(lanelengths[laneIndex]))
                else:
                    radius = 0
                    angle = 0
                
                if 'switch' in piece :
                    hasSwitch = 1
                else : 
                    hasSwitch = 0
                
                #Print info: Index;Length;Angle;Radius;HasSwitch;Lanedistance1;LaneDistance2;...
                #pieceInfoString = str(pieceIndex) + ";" + str(length) + ";" + str(radius) + ";" + str(angle) + ";" + str(hasSwitch)
                #for lane in lanelengths :
                #    pieceInfoString = pieceInfoString + ";" + str(lanelengths[lane])
                #print(pieceInfoString)
                
                #Put values in memory
                self.pieces.append([length, radiuslanes, angle, lanelengths, hasSwitch])
                pieceIndex = pieceIndex + 1 #Next piece index
        else: # wtf?
            self.pieces.append([-1, -1, -1])
        
        self.numberOfPieces = pieceIndex
        
        #print("Cars")
        # Read the cars
        if 'cars' in data['race']:
            for car in data['race']['cars'] :
                if car['id']['color'] != self.mycolor :
                    self.enemies[car['id']['color']] = [ 0, 0, 0, 0, 0, 0, 0]
                    self.enemySlow[car['id']['color']] = 0
                    self.enemiesDnf[car['id']['color']] = 0
                self.cars[car['id']['color']] = [ car['id']['name'], car['id']['color'], car['dimensions']['length'], car['dimensions']['width'], car['dimensions']['guideFlagPosition'] ]
                #Print car info: name;color;length;width;guideflagposition
                #print(str(car['id']['name']) + ";" + str(car['id']['color']) + ";" + str(car['dimensions']['length']) + ";" + str(car['dimensions']['width']) + ";" + str(car['dimensions']['guideFlagPosition']))
                
                
                
        # Read the race session
        #if 'raceSession' in data['race'] :
        #    self.laps = data['race']['raceSession']['laps']
        #    self.maxLapTimeMs = data['race']['raceSession']['maxLapTimeMs']
        #    if data['race']['raceSession']['quickRace'] == 'true' :
        #        self.quickRace = True
        #    else :
        #        self.quickRace = False
        
        #print("Straights")
        
        pieceIndex = 0
        maxlength = 0
        #Find first straight beginning after start
        if(self.pieces[self.numberOfPieces - 1][0] > 0) : #If the first piece is not beginning of straight
            #Find next bend
            while self.pieces[pieceIndex][0] > 0 :
                pieceIndex = pieceIndex + 1
            #Find next beginning of straight
            while abs(self.pieces[pieceIndex][2]) > 0 :
                pieceIndex = pieceIndex + 1
        
        #Loop through all pieces
        while pieceIndex < self.numberOfPieces :
                
            straightlength = 0 #Initiliaze straight length counter
            nextIndex = pieceIndex #Initial loop index to current piece
            while self.pieces[nextIndex][0] > 0 : #Loop until next bend
                straightlength = straightlength + self.pieces[nextIndex][0] #Add lengths of straight pieces together
                nextIndex = nextIndex + 1 #Next piece to check
                if(nextIndex > self.numberOfPieces - 1 ) : #If pieces go over end of lap
                    nextIndex = 0 #Back to first piece
            
            self.straights[pieceIndex] = straightlength #Remember straight length
            
            #print(str(pieceIndex) + " - " + str(straightlength)) #Print straight starting index and length
            
            if straightlength > maxlength : #If straight was longer than previous longest straight
                maxlength = straightlength #Remember length
                self.maxStraightPieceIndex = pieceIndex #Remember start index
                
            while abs(self.pieces[nextIndex][2]) > 0 : #Loop until next straight piece
                nextIndex = nextIndex + 1 #Next piece to check
                if(nextIndex > self.numberOfPieces - 1 ) : #If pieces go over end of lap
                    nextIndex = 0 #Back to first piece
            
            if pieceIndex < nextIndex : #If in same lap (check index has not gone over finish line, back to beginning)
                pieceIndex = nextIndex #New start straigth piece index is latest check index
            else :
                pieceIndex = self.numberOfPieces #Piece index high enough to finish the loop
         
        #print("MAX : " + str(self.maxStraightPieceIndex) + " - " + str(self.straights[self.maxStraightPieceIndex])) #Print max
        
        #Lap lengths
        #print("LANE")
        for lane in self.lanes :
            laplength = 0
            for piece in range(self.numberOfPieces) :
                if self.pieces[piece][0] > 0 :
                    laplength = laplength + self.pieces[piece][0]
                else :
                    laplength = laplength + self.pieces[piece][3][lane]
            self.laplengths.append( laplength )
            #print(str(lane) + " : " + str(laplength) )
                
        #print("Track info ends")        
        
        #self.ping()
    
    # * During the race
    def on_car_positions(self, data): #, gametick):
        #Get info from data structure
        for car in range(len(data)) : #Loop through all cars in data
            if data[car]['id']['color'] == self.mycolor : #If car is my car
                #Get position values
                angle = data[car]['angle']
                pieceIndex = data[car]['piecePosition']['pieceIndex']
                pieceDistance = data[car]['piecePosition']['inPieceDistance']
                startLane = data[car]['piecePosition']['lane']['startLaneIndex']
                endLane = data[car]['piecePosition']['lane']['endLaneIndex']
            else:
                #Angular velocity
                self.enemies[data[car]['id']['color']][1] = data[car]['angle'] - self.enemies[data[car]['id']['color']][0]
                #Angle
                self.enemies[data[car]['id']['color']][0] = data[car]['angle']
                #Speed
                if data[car]['piecePosition']['pieceIndex'] == self.enemies[data[car]['id']['color']][2] : #If current piece of track is the same as at previous tick
                    self.enemies[data[car]['id']['color']][4]  = data[car]['piecePosition']['inPieceDistance'] - self.enemies[data[car]['id']['color']][3]
                elif self.pieces[data[car]['piecePosition']['pieceIndex']][0] > 0 : #If piece is a straight line (length exists)
                    self.enemies[data[car]['id']['color']][4]  = data[car]['piecePosition']['inPieceDistance'] + self.pieces[data[car]['piecePosition']['pieceIndex']][0] - self.enemies[data[car]['id']['color']][3]
                else : #If previous piece was a curve (length of curve depends on the lane the car is on)
                    self.enemies[data[car]['id']['color']][4]  = data[car]['piecePosition']['inPieceDistance'] + self.pieces[data[car]['piecePosition']['pieceIndex']][3][self.currentLane] - self.enemies[data[car]['id']['color']][3]
                #PieceIndex
                self.enemies[data[car]['id']['color']][2] = data[car]['piecePosition']['pieceIndex']
                #Distance
                self.enemies[data[car]['id']['color']][3] = data[car]['piecePosition']['inPieceDistance']
                
                self.enemies[data[car]['id']['color']][5] = data[car]['piecePosition']['lane']['startLaneIndex']
                self.enemies[data[car]['id']['color']][6] = data[car]['piecePosition']['lane']['endLaneIndex']
                
               
                
        #On first tick, speed (and acceleration) is zero (but pieceindex and distance might not be, if not at first starting position)
        if self.onCarPositionsFirstTime :
            speed = 0
            for enemy in self.enemies :
                self.enemies[enemy][4] = 0
        else :
            #Calculate speed as derivative of distance per game tick 
            # XXX Doesn't take in account the track changes
            if pieceIndex == self.pieceIndex : #If current piece of track is the same as at previous tick
                speed = pieceDistance - self.pieceDistance
            elif self.pieces[self.pieceIndex][0] > 0 : #If piece is a straight line (length exists)
                speed = pieceDistance + self.pieces[self.pieceIndex][0] - self.pieceDistance
            else : #If previous piece was a curve (length of curve depends on the lane the car is on)
                speed = pieceDistance + self.pieces[self.pieceIndex][3][self.currentLane] - self.pieceDistance
        
        
            
        #Calculate angle, angular velocity and angular acceleration
        angularVelocity = angle-self.angle
        
        if not self.hasSetC123 and abs(angle) > 0 :
            #if self.setC123counter < 1 :
            #    self.setC123radius = self.pieces[self.pieceIndex][1][self.currentLane]
            if self.setC123counter < self.amountOfEstimates :
                #Remember measurement/calculation values 
                self.setC123angleCurve.append(self.pieces[self.pieceIndex][2])
                if self.setC123angleCurve[self.setC123counter]:
                    self.setC123radius.append(self.pieces[self.pieceIndex][1][self.currentLane])
                else:
                    self.setC123radius.append(0)
                self.setC123angle.append( angle )
                self.setC123angularVelocity.append( angularVelocity )
                self.setC123angularAcceleration.append( angularVelocity - self.angularVelocity )
                self.setC123velocity.append( self.speed )#self.speed
                #Next tick
                self.setC123counter = self.setC123counter + 1
            if self.setC123counter == self.amountOfEstimates : #If has values of four ticks in memory
                if self.use_matrix:
                    C123matrix = np.matrix([[ self.setC123angle[0] , self.setC123angularVelocity[0] , (self.setC123velocity[1] * self.setC123velocity[1] - self.setC123velocity[0] * self.setC123velocity[0] ) / self.setC123radius[0] ], 
                                          [ self.setC123angle[1] , self.setC123angularVelocity[1] , (self.setC123velocity[2] * self.setC123velocity[2] - self.setC123velocity[0] * self.setC123velocity[0] ) / self.setC123radius[0] ],
                                          [ self.setC123angle[2] , self.setC123angularVelocity[2] , (self.setC123velocity[3] * self.setC123velocity[3] - self.setC123velocity[0] * self.setC123velocity[0] ) / self.setC123radius[0] ]])
                    C123vector = np.matrix([ [ self.setC123angularAcceleration[1] - self.setC123angularAcceleration[0] ], [ self.setC123angularAcceleration[2] - self.setC123angularAcceleration[0] ] , [ self.setC123angularAcceleration[3] - self.setC123angularAcceleration[0] ] ])
                    
                    C123matrixinverse = np.linalg.inv(C123matrix)
                    C123result = np.dot(C123matrixinverse, C123vector)
                    self.C1 = C123result[0,0]
                    self.C2 = C123result[1,0]
                    self.C3 = C123result[2,0]
                    self.zeroForce = - self.setC123angularAcceleration[0] / self.C3 + self.setC123velocity[0] * self.setC123velocity[0] / self.setC123radius[0]    
        #print("radius2: " + str(self.setC123counter) )
        #print("radius3: " + str(self.C123_iterations) )   
        if not self.hasSetC123 and self.setC123counter == self.amountOfEstimates :

                bestC1 = self.C1
                bestC2 = self.C2
                bestC3 = self.C3
                bestZeroForce = self.zeroForce
                bestC4 = self.C4
                Cs  = [bestC1,bestC2,bestC3,bestZeroForce]
                    #print(str(bestC4))
                    #print(str([bestC1,bestC2,bestC3,bestZeroForce]+bestC4))
                phisBest   = calculate_phis2.calculate_phis2(self.setC123velocity, self.setC123radius, self.setC123angleCurve,Cs, self.setC123angle, self.setC123angularVelocity)
#                     alphasBest = []
#                     for i in range(self.amountOfEstimates):  
#                             alphasBest.append(throttleFunctions.calculate_angular_acceleration(bestC1,bestC2,bestC3,bestZeroForce,self.setC123angularVelocity[i],
#                                                                           self.setC123angle[i],self.setC123angleCurve[i],self.setC123velocity[i], self.setC123angularVelocity[i],bestC4))
                    #print(str(bestC4))
                randFactor = [0.035,0.35]
                for ind in range(self.guesses):
                    Cs = throttleFunctions.guessCs2([bestC1,bestC2,bestC3,bestZeroForce],randFactor)
                    #print(str(Cs))
                        #Cs = throttleFunctions.guessCs([bestC1,bestC2,bestC3,bestZeroForce]+bestC4,self.randomizeCs)
                        #alphasGuess = []
                        #for ind in range(self.guesses):
                        #Cs = throttleFunctions.guessCs([bestC1,bestC2,bestC3,bestZeroForce]+bestC4,self.randomizeCs)
                        #alphasGuess = []
                        #print(str(Cs[4]))
                    for j in range(len(Cs)):
                        if Cs[j] < self.lowerLimitsC123[j]:
                            Cs[j] = self.lowerLimitsC123[j]
                        elif Cs[j] > self.upperLimitsC123[j]:
                            Cs[j] = self.upperLimitsC123[j]
                        #print("moi" + str([bestC1,bestC2,bestC3,bestZeroForce]+bestC4))
                        #print(str(bestC4))
                         
#                         for i in range(self.amountOfEstimates):
#                             alphasGuess.append(throttleFunctions.calculate_angular_acceleration(Cs[0],Cs[1],Cs[2],Cs[3],self.setC123angularVelocity[i],
#                                                                           self.setC123angle[i],self.setC123angleCurve[i],self.setC123velocity[i], self.setC123angularVelocity[i],Cs[3:-1]))
#                         testIfBetter = throttleFunctions.compare(self.setC123angularAcceleration,alphasBest,alphasGuess)
                        
                    phisGuess    = calculate_phis2.calculate_phis2(self.setC123velocity, self.setC123radius, self.setC123angleCurve,Cs, self.setC123angle, self.setC123angularVelocity)
                    testIfBetter = throttleFunctions.compare(self.setC123angle,phisBest,phisGuess)
                    if testIfBetter:
                        phisBest = phisGuess
                        bestC1 = Cs[0]
                        bestC2 = Cs[1]
                        bestC3 = Cs[2]
                        bestZeroForce = Cs[3]
                        #print("moi"+str([bestC1,bestC2,bestC3,bestZeroForce]+bestC4))
                            #bestC4 = Cs[3:-1]
                            #testBest = testIfBetter
                        #print("radius: " + str(testIfBetter) )
                    #print("rrrr: " + str(bestC1) )
                self.C1 = bestC1
                self.C2 = bestC2
                self.C3 = bestC3
                self.zeroForce = bestZeroForce
                self.C4 = bestC4
                self.C123_iterations = self.C123_iterations+1
                if self.C123_iterations == self.iteration_amounts :
                    self.maxAngle = 37
                    
                    self.hasSetC123 = True
                    
                
  
        self.angularAcceleration = angularVelocity-self.angularVelocity
        self.angularVelocity = angularVelocity
        self.angle = angle
        
        #If car has switched lanes
        if not self.onCarPositionsFirstTime :
            if startLane != self.currentLane :
                self.switchTo = "" 
                speed = speed + 2.06 #Switching increases distance with 2.06
        
        self.currentLane = startLane
        self.nextLane = endLane
        
        #Calculate acceleration (derivative of speed per game tick)
        if self.acceleration == 0 and not self.hasSetCT and not speed == 0:
            self.CT = speed / self.throttlevalue
            self.hasSetCT = True
        if not self.hasSetCv :
            self.hasSetCv_ind = self.hasSetCv_ind + 1
        if self.hasSetCv_ind == 10 and not self.hasSetCv:
            self.hasSetCv = True
            self.Cv = ((speed - self.speed)-self.CT * self.throttlevalue)/self.speed
        self.acceleration = speed - self.speed
        self.speed = speed #Remember new speed

        #Remember new location
        self.pieceDistance = pieceDistance
        self.pieceIndex = pieceIndex
        
        #print("CARPOS Angle: " + str(angle) + ", Speed: " + str(self.speed) + " , PiecePos: " + str(pieceIndex) + "/" + str(pieceDistance) + ", Lane: " + str(startLane) + "/" + str(endLane) )
        
        #Print car info: Throttle;Angle;Speed;ation;PieceIndex;PieceDistance;StartLane;EndLane
        #print(str(self.throttlevalue) + ";" + str(angle) + ";" + str(self.angularVelocity) + ";" + str(self.angularAcceleration) + ";" + str(self.speed) + ";" + str(self.acceleration) + ";" + str(pieceIndex) + ";" + str(pieceDistance) + ";" + str(startLane) + ";" + str(endLane) )
        
        
        #print("NEXT SWITCH : " + str(self.distance_to_next_switch(self.pieceIndex,self.pieceDistance,self.currentLane)))
        
        #for car in self.enemies :
        #    print("ENEMY " + str(car) + " : " + str(self.enemies[car][0]) + " / " + str(self.enemies[car][1]) + " / " + str(self.enemies[car][2]) + " / " + str(self.enemies[car][3]) + " / " + str(self.enemies[car][4]) + " / " + str(self.enemies[car][5]) + " / " + str(self.enemies[car][6]))
        #    print(str(self.enemy_distance_front(car)) + " / " + str(self.enemy_distance_behind(car)) )
        #    print(str(self.enemy_going_to_hit_front(car)))
        
        #for looplane in self.lanes :
        #    print("FINDENEMY " + str(looplane) + " : " + str(self.enemy_front(looplane)) + " / " + str(self.enemy_behind(looplane)))
        
        
        
        self.gametickcalc = self.gametickcalc + 1
        
        commandGiven = False
        
        if self.speed < 2:
            self.throttle(1.0)
            commandGiven = True
        
        # *** Use Turbo ***
        if commandGiven == False :
            if self.turboAvailable == True :
                if self.pieceIndex == self.maxStraightPieceIndex : #and self.angle < 30:
                    self.useTurbo()
                    commandGiven = True
        
        
     
        #if( False ) :
        if(commandGiven == False):
            if( self.switchCalculatedIndex + self.numberOfPieces * (self.switchCalculateLap - self.currentLap) <= self.pieceIndex ) :
                
                self.nextLaneDefaultDecision = self.checkNextSwitch()
                
                #print("SCI: " + str(self.switchCalculatedIndex) + ";" + str(nextSwitchIndex) + ";" + str(self.pieceIndex) + ";" + str(self.pieceIndex % self.numberOfPieces) + ";" + str(self.numberOfPieces) )
        
        
        if commandGiven == False :
            commandGiven = self.enemy_controller()
        
        #if commandGiven == False :
            
        #    if self.gametickcalc < 100 :
        #        self.throttle(0.8)
        #        commandGiven = True
        #    elif self.gametickcalc < 500 :
        #        self.throttle(0.2)
        #        commandGiven = True
        
        
        
        if (commandGiven == False) :  
                self.throttle(self.bot_throttle())
                commandGiven = True
        
        if ( self.turboInUse ) :
            self.reduceTurbo()
            
        if self.onCarPositionsFirstTime :
            self.onCarPositionsFirstTime = False
            
        #if (commandGiven == False) :  
        #    self.throttle(self.bot_throttle())
  
    def on_crash(self, data):
        print("CRASH name: " + data['name'] + ", color:" + data['color'] )
        if data['color'] == self.mycolor : #If crashed car is mine
            self.ontrack= 0 #Car is no longer on track
            #Turbo in use is lost
            
        self.ping()
        
    def on_spawn(self, data):
        print("SPAWN name: " + data['name'] + ", color: " + data['color'] )
        if data['color'] == self.mycolor : #If spawned car is mine
            self.ontrack = 1 #Car is back on the track
            self.throttle(1.0) #Speed up 
    
    def on_turbo_available(self, data):
        print("TURBO AVAILABLE ms: " + str(data['turboDurationMilliseconds']) + ", ticks: " + str(data['turboDurationTicks']) + ", factor: " + str(data['turboFactor']) )
        self.turboAvailable = True
        self.turboAvailableFactor = float( data['turboFactor'] )
        self.turboAvailableTicksMax = float( data['turboDurationTicks'] )
        self.ping()
        
    def on_lap_finished(self, data):
        self.currentLap = self.currentLap + 1
        print("Lap finished")
        self.ping()
    
    # * Finishing a race    
    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()
  
    def on_dnf(self, data):
        self.enemiesDnf[data['car']['color']] = 1
        self.ping()
  
    # *** Auxiliary functions ***
    
    # * Server to Bot message reading loop *
    def msg_loop(self):
        msg_map = {
            'on_join': self.on_join,
            'gameStart': self.on_game_start,
            'yourCar': self.on_your_car,
            'gameInit': self.on_game_init,
            'carPositions': self.on_car_positions,
            'turboAvailable': self.on_turbo_available,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'lapFinished': self.on_lap_finished,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'dnf': self.on_dnf,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line) #Load text line in JSON format into a Python data structure
            msg_type, data = msg['msgType'], msg['data'] #Read type and data in to structures
            if msg_type in msg_map:
                #if msg_type == 'carPositions' :
                #    msg_map[msg_type](data, msg['gametick']) 
                #else :
                    msg_map[msg_type](data) 
            else:
                print("Got {0}".format(msg_type))
                print("MSG: {0}".format(data))
                self.ping()
            line = socket_file.readline()
    
    # * Bot responses to server *
    
    #Change throttle to given value
    def throttle(self, throttle):
        self.throttlevalue = throttle #Remember the throttle
        self.msg("throttle", throttle)
    #Change lanes
    def switchLaneLeft(self):
        self.msg("switchLane", "Left")
    def switchLaneRight(self):
        self.msg("switchLane", "Right")
    #Use turbo
    def useTurbo(self):
        print("USE TURBO - HERE WE GO!")
        #Turbo in use values
        self.turboInUse = True
        self.turboFactor = self.turboAvailableFactor
        self.turboTicksLeft = self.turboAvailableTicksMax
        #Update available turbo values (it has been used)
        self.turboAvailable = False
        self.turboAvailableFactor = 0
        self.turboAvailableTicksMax = 0
        #Turbo message to server
        self.msg("turbo","Here we go") #Msgtype has to be "turbo" but data can be anything
        
    def reduceTurbo(self):
        self.turboTicksLeft = self.turboTicksLeft - 1
        if self.turboTicksLeft <= 0 :
            self.turboInUse = False
            self.turboFactor = 0
        
    #Send ready to join message
    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})
    #Create new race with chosen track, password and number of cars in the race    
    def create_race(self, trackName, password, carCount):
        self.msg("createRace", {"botId": { "name": self.name, "key": self.key }, "trackName": trackName, "password": password, "carCount": carCount } )
    
    #Join a race created by another competitor
    def join_race(self, carCount, trackName="", password="", ):
        if trackName == "" : #Track name is not given
            self.msg("joinRace", {"botId": { "name": self.name, "key": self.key }, "carCount": carCount } ) #Join public race with given car count
        else : #Join race with given track, password (can be empty) and car count
            self.msg("joinRace", {"botId": { "name": self.name, "key": self.key }, "trackName": trackName, "password": password, "carCount": carCount } )
            
    #Do nothing   
    def ping(self):
        self.msg("ping", {})
    
    # * Messaging *
    
    #Create JSON message
    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))
    #Send JSON message through socket
    def send(self, msg):
        self.socket.send(msg + "\n")
    
tttMasterBot.calulate_distance = calculate_distance.calculate_distance
tttMasterBot.bot_throttle = bot_throttle.bot_throttle
tttMasterBot.piece_length = throttleFunctions.piece_length

tttMasterBot.enemy_front = enemyFunctions.enemy_front
tttMasterBot.enemy_behind = enemyFunctions.enemy_behind
tttMasterBot.enemy_distance_front = enemyFunctions.enemy_distance_front
tttMasterBot.enemy_distance_behind = enemyFunctions.enemy_distance_behind
tttMasterBot.enemy_going_to_hit_front = enemyFunctions.enemy_going_to_hit_front
tttMasterBot.distance_to_next_switch = enemyFunctions.distance_to_next_switch
tttMasterBot.distance_to_next_switch_front = enemyFunctions.distance_to_next_switch

tttMasterBot.checkNextSwitch = laneFunctions.checkNextSwitch
tttMasterBot.enemy_controller = enemy_controller.enemy_controller

#Main
if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = tttMasterBot(s, name, key)
        bot.run()
