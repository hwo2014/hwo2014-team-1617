import calculate_phis2
import numpy as np
import throttleFunctions
import enemyFunctions
import bot_throttle
import laneFunctions

def enemy_controller(self):
    switchDecision = self.nextLaneDefaultDecision
    currentLane = self.currentLane
    
    if len(self.enemies) > 0 :
    
        
        enemy_front_lane = enemyFunctions.enemy_front(self, currentLane)
        if not enemy_front_lane == "" :
            #enemy_bedind_lane = enemyFunctions.enemy_behind(self,currentLane)
            enemy_front_here  = enemyFunctions.enemy_distance_front(self,enemy_front_lane)
            distance_to_switch = enemyFunctions.distance_to_next_switch_front(self,self.pieceIndex,self.pieceDistance,currentLane)
            if distance_to_switch < self.speed + 3.0 :
                # *** Lane we should go ***
                enemyInFront = enemyFunctions.enemy_front(self,self.currentLane + switchDecision[0])
                if enemyInFront != "" :
                    frontenemydistance = enemyFunctions.enemy_distance_front(self,enemyInFront)
                    #If enemy is far enough 
                    if frontenemydistance > 60 + 5 * ( self.speed - self.enemies[enemyInFront][4] ) and  switchDecision[0]:
                        if not (enemy_front_here < distance_to_switch) or not self.enemySlow[enemy_front_lane] :
                            #"
                            laneFunctions.switchBasedOnDecision(self,switchDecision[0])
                            return True
                    if enemy_front_here < 60 + 5 * ( self.speed - self.enemies[enemy_front_lane][4] ) and not switchDecision[0] and self.enemySlow[enemy_front_lane] :
                        laneFunctions.switchBasedOnDecision(self,switchDecision[1])
                        return True  
                else:
                     laneFunctions.switchBasedOnDecision(self,switchDecision[0])
                     return True          
               
            thr0 = bot_throttle.bot_throttle(self)
            hit = enemyFunctions.enemy_going_to_hit_front(self, enemy_front_lane,thr0)
            if not hit:
                self.throttle(thr0)
                return True
            if enemyFunctions.distance_to_next_switch(self,self.pieceIndex,self.pieceDistance,currentLane) - hit < 0 :
                self.throttle(thr0)
                return True
            self.enemySlow[enemy_front_lane] = self.enemySlow[enemy_front_lane] + 1
            for thr in np.linspace(thr0,0,11) :
                hit = enemyFunctions.enemy_going_to_hit_front(self, enemy_front_lane,thr)
                if not hit:
                    self.throttle(thr0)
                    return True
            self.throttle(0.0)
            return True
        
        else : #If nobody
            #enemy_front_here  = enemyFunctions.enemy_distance_front(self,enemy_front_lane)
            distance_to_switch = enemyFunctions.distance_to_next_switch_front(self,self.pieceIndex,self.pieceDistance,currentLane)
            if distance_to_switch < self.speed + 3.0 :
                # *** Lane we should go ***
                enemyInFront = enemyFunctions.enemy_front(self,self.currentLane + switchDecision[0])
                if enemyInFront == "" and switchDecision[0]:
                    laneFunctions.switchBasedOnDecision(self,switchDecision[0])
                    return True
                elif enemyInFront == "" :
                    return False
                frontenemydistance = enemyFunctions.enemy_distance_front(self,enemyInFront)
                #If enemy is far enough 
                if frontenemydistance > 60 + 5 * ( self.speed - self.enemies[enemyInFront][4] ) and  switchDecision[0]:
                    laneFunctions.switchBasedOnDecision(self,switchDecision[0])
                    return True
            
                
            return False
    
    else : #If no enemies on track
        distance_to_switch = enemyFunctions.distance_to_next_switch_front(self,self.pieceIndex,self.pieceDistance,currentLane)
        if switchDecision[0] and distance_to_switch < self.speed + 3.0 :
            laneFunctions.switchBasedOnDecision(self,switchDecision[0])
            return True
        return False
    
    
