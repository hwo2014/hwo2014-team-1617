
import throttleFunctions

def stop_distance(Cv , v0):
    return -v0/Cv*1.01
    


def enemy_front(self, drivingline): #Get closest enemy in front in given lane
    enemyColor = ""
    enemyMinDistance = 100000
    for enemy in self.enemies :
        if self.enemies[enemy][5] == drivingline and self.enemiesDnf[enemy] == 0 :
            enemyDistance = enemy_distance_front(self, enemy)
            if enemyMinDistance > enemyDistance:
                enemyMinDistance = enemyDistance
                enemyColor = enemy
    return enemyColor

def enemy_behind(self, drivingline): #Get closest enemy at behind in given lane
    enemyColor = ""
    enemyMinDistance = 100000
    for enemy in self.enemies :
        if self.enemies[enemy][5] == drivingline and self.enemiesDnf[enemy] == 0 :
            enemyDistance = enemy_distance_behind(self, enemy)
            if enemyMinDistance > enemyDistance:
                enemyMinDistance = enemyDistance
                enemyColor = enemy
    return enemyColor

def enemy_distance_front(self, enemyColor):
    enemydistance = 0
    if self.enemiesDnf[enemyColor] == 1 :
        return self.laplengths[self.currentLane] / 2
    
    if self.pieceIndex == self.enemies[enemyColor][2] : #If on same piece
        if self.pieces[self.pieceIndex][0] > 0 : #If straight piece
            enemydistance = self.enemies[enemyColor][3] - self.pieceDistance
        else : #If curve, enemy's piecedistance is 
            enemydistance = self.enemies[enemyColor][3] / self.pieces[self.enemies[enemyColor][2]][3][self.enemies[enemyColor][5]] * self.pieces[self.pieceIndex][3][self.currentLane] - self.pieceDistance
            
        if enemydistance < 0 : #If enemy is behind you, add all other pieces lengths
            for piece in range(self.numberOfPieces) :
                if piece != self.pieceIndex :
                    if self.pieces[piece][0] > 0 : #If straight piece
                        enemydistance = enemydistance + self.pieces[piece][0]
                    else: #If curve
                        enemydistance = enemydistance + self.pieces[piece][3][self.currentLane]
    else :
        #Distance from own location to end of own piece
        if self.pieces[self.pieceIndex][0] > 0 :
            enemydistance = self.pieces[self.pieceIndex][0] - self.pieceDistance
        else :
            enemydistance = self.pieces[self.pieceIndex][3][self.currentLane] - self.pieceDistance
        
        #Distances in pieces between
        loopIndex = self.pieceIndex + 1
        if loopIndex >= self.numberOfPieces : #Loop through all pieces between and add them to distance
            loopIndex = 0
        while loopIndex != self.enemies[enemyColor][2] :
            if self.pieces[loopIndex][0] > 0 :
                enemydistance = enemydistance + self.pieces[loopIndex][0]
            else:
                enemydistance = enemydistance +  self.pieces[loopIndex][3][self.currentLane]
            loopIndex = loopIndex + 1
            if loopIndex >= self.numberOfPieces :
                loopIndex = 0
        #Distance in enemy's piece    
        if self.pieces[self.enemies[enemyColor][2]][0] > 0 :
            enemydistance = enemydistance + self.enemies[enemyColor][3]
        else :
            enemydistance = enemydistance + self.enemies[enemyColor][3] / self.pieces[self.enemies[enemyColor][2]][3][self.enemies[enemyColor][5]] * self.pieces[self.enemies[enemyColor][2]][3][self.currentLane]
        
    return enemydistance

def enemy_distance_behind(self, enemyColor): #Distance behind = Full lap length - Distance front
    return (self.laplengths[self.currentLane] - enemy_distance_front(self, enemyColor))
    

def enemy_going_to_hit_front(self, enemyColor,throttle): #Return 0 if no hit, 1 if hit and enemy in front, -1 if hit and enemy behind
    car_lens = self.cars[self.mycolor][2]
    if self.currentLane != self.enemies[enemyColor][5] : #If enemy on different lane
        return 0 #No hit
    else :
        if self.turboInUse :
            turboFactor = self.turboFactor
        else:
            turboFactor = 1
        nextSpeed = self.speed + throttleFunctions.calculate_acceleration(self.Cv, self.CT, self.speed, throttle, turboFactor)    
        enemySpeed = self.enemies[enemyColor][4] #+ throttleFunctions.calculate_acceleration(self.Cv, self.CT, self.enemies[enemyColor][4], 0, 0)
        
        front_distance_to_enemy = enemy_distance_front(self, enemyColor) - nextSpeed - stop_distance(self.Cv,nextSpeed) + stop_distance(self.Cv,enemySpeed) - car_lens
        if front_distance_to_enemy <= 0 :
            return enemy_distance_front(self, enemyColor) + stop_distance(self.Cv,enemySpeed)
        else : 
            behind_distance_to_enemy = enemy_distance_behind(self, enemyColor) + stop_distance(self.Cv,self.speed) - stop_distance(self.Cv,enemySpeed) - car_lens
            if behind_distance_to_enemy <= 0 :
                return -enemy_distance_behind(self, enemyColor) - stop_distance(self.Cv,self.speed)
            else:
                return 0 #No hit

def distance_to_next_switch(self,pIndex,pDistance,pLane):
    if self.pieces[pIndex][4]:
        return self.pieces[pIndex][3][pLane] - pDistance
    distance = self.pieces[pIndex][3][pLane] - pDistance
    for i in range(self.numberOfPieces):
        pIndex = throttleFunctions.next_piece_index(pIndex, self.numberOfPieces)
        distance = distance + self.pieces[pIndex][3][pLane]
        if self.pieces[pIndex][4]:
            break
    return distance
    

def distance_to_next_switch_front(self,pIndex,pDistance,pLane):
    #if self.pieces[pIndex][4]:
    #    return self.pieces[pIndex][3][pLane] - pDistance
    distance = self.pieces[pIndex][3][pLane] - pDistance
    for i in range(self.numberOfPieces):
        pIndex = throttleFunctions.next_piece_index(pIndex, self.numberOfPieces)
        if self.pieces[pIndex][4]:
            break
        distance = distance + self.pieces[pIndex][3][pLane]
        
    return distance

